# RL with AAL
![](https://img.shields.io/badge/Status-working-success)
![](https://img.shields.io/badge/Repository_Version-0.3-success)
![](https://img.shields.io/badge/Python%20Version-3.10.6-blue)

## Description

This repository contains the python-code for: 
- the simulation-environment of the achromatic adaptive lens (AAL) called `gym_AAL`
- registration of the environment `gym-AAL` as a gym-enviroment for easier use
- training file [train.py](train.py)
- simulation notebook [simulation.ipynb](simulation.ipynb)
- saved models in [model](./model/)
- training results in [train_results](./train_results/)

[![](https://mermaid.ink/img/pako:eNpNzsEKwjAMBuBXKTkpbC_Qg9Axz4J6sx7iGreibUfWTWTbu1udgjmF_F9IRqiCIZBQM7aNOJbai1TqpGry8SzyfDOpKtrgJ1Gstn6wHLxL0XqBxUfs6YFsJqH-h7tLRzzgsqsgA0fs0Jp0bHw7DbEhRxpkag3yTYP2c3J9azDS1tgYGOQV7x1lgH0Mh6evQEbu6YdKi-lx91XzC4DuQ78)](https://mermaid-js.github.io/mermaid-live-editor/edit#pako:eNpNzsEKwjAMBuBXKTkpbC_Qg9Axz4J6sx7iGreibUfWTWTbu1udgjmF_F9IRqiCIZBQM7aNOJbai1TqpGry8SzyfDOpKtrgJ1Gstn6wHLxL0XqBxUfs6YFsJqH-h7tLRzzgsqsgA0fs0Jp0bHw7DbEhRxpkag3yTYP2c3J9azDS1tgYGOQV7x1lgH0Mh6evQEbu6YdKi-lx91XzC4DuQ78)

# Installation

First of all clone the repository via SSH or HTTPS.
Afterwards, create and activate a virtual environment to install packages locally, please refer to this [guide](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment). Then install the neccessary packages via pip: `pip install -r requirements.txt`

**_Note:_**_If you use Anaconda as the interpreter, please follow this [guide](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)._

**_Note:_**_If there is any error massage during installing the requirements, please use `pip install -r requirements_v1.txt` instead of  `pip install -r requirements.txt`._

## Register the environment

1. Make sure, the folder called `gym-AAL` is placed in your current project folder
2. The script [setup.py](setup.py) has to be placed in the same project folder
3. run the command `pip install -e .`
4. now you can use the enviroment in your training file as you would do with a common gym-environment:  
``` ruby
import gym
env = gym.make('AAL-v0')
```
5. If neccessary add a Monitor wrapper ` env = Monitor(gym.make('AAL-v0'))` or vectorize the environment `env = make_vec_env('AAL-v0', n_envs = 8)`

# Usage

## Train the agent

The file [train.py](train.py) trains a Recurrent PPO agent (PPO agent with shared neural networks for actor and critic, additional lstm layer in the actor part). The agent derives from [stable-baseline3 contrib](https://github.com/Stable-Baselines-Team/stable-baselines3-contrib).
The network architecture, hyperparameters e.g. learning rate can be changed directly in [train.py](train.py).

## Monitor the training process

The training has tensorboard support integrated. You can monitor the training by calling tensorboard and running it in your browser or editor: 
```
tensorboard --logdir ./train_results
```

## Simulate one episode with a trained agent

Once an agent is trained and the model file is stored, it can be run through one episode of the environment and the actions and resulting chromatic aberration with a random start chromatic aberration are plotted. For this please use the file [./simulation/simulation.ipynb](./simulation/simulation.ipynb) and change the model file accordingly.


# Results
The agents PPO_1 to PPO_5 have been trained with different hyperparameters:  
|  	| PPO_1 	| PPO_2 	| PPO_3 	| PPO_4 	| PPO_5 	|
|---	|---	|---	|---	|---	|---	|
| learning rate 	| 1e-4 	| 1e-3 	| 1e-3 	| 1e-3 	| 1e-3 	| 
| clip range 	| 0.2 	| 0.18 	| 0.2 	| 0.2 	| 0.2 	| 
| discount factor 	| 0.95	| 0.95 	| 0.95 	| 0.99 	| 0.995 	|

The performance of the agent PPO_3 looks like this in the end:  

<img src="./simulation/sim_result.svg" alt="drawing" width="500" />

The test in the lab with agent PPO_3 was done using the script [validation.ipynb](validation.ipynb) and showed these results:  
<img src="./validationPlot/abs_PPO_3_Thyroid4_2_withNewMsg.svg" alt="drawing" width="500" />

## Support
:computer: Katharina Schmidt (katharina.schmidt1@tu-dresden.de)

## Authors and acknowledgment
:bulb: Nektarios Koukourakis  
:computer: Ning Guo  
:computer: Katharina Schmidt  

