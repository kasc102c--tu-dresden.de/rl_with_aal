import gym

gym.envs.register(
    id='AAL-v0',
    entry_point='gym_AAL.envs:AAL',
    max_episode_steps = 500,
)