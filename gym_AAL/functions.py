import numpy as np
import math

def displacement_one_piezo(p):
    d_init = -274e-12
    Alpha = -2.13e-12
    D = 0.0

    #check if voltage changed
    if p['Action']==p['X1']:
        D=p['Y1']
        return D

    #check if first input of episode    
    if p['Startup_flag']==0:
        if p['Action']>0.0:
            p['Startup_flag']=1
            p['Case_flag']=1
            p['TP_flag']=0
            p['X0']=-p['Action']
            p['V0'] = abs(p['Action'])
            p['Y0'] =Displacement(d_init, Alpha, p['V0'], p['X0'], p['Case_flag'])
            p['X_LTP'][0]=p['X0']
            p['Y_LTP'][0]=Displacement(d_init, Alpha, p['V0'], p['X0'], p['Case_flag'])
        else:
            p['Startup_flag']=1
            p['Case_flag']=2
            p['TP_flag']=1
            p['X0']=-p['Action'] 
            p['V0']=abs(p['Action'])
            p['Y0']=Displacement(d_init, Alpha, p['V0'], p['X0'], p['Case_flag'])
            p['X_RTP'][0]=p['X0']
            p['Y_RTP'][0]=Displacement(d_init, Alpha, p['V0'], p['X0'], p['Case_flag'])

    if (p['Action']-p['X1'])*(p['X1']-p['X2'])<0:
        if p['Action']-p['X1']<0: 
            p['X_RTP'][p['Index']]=p['X1']
            p['Y_RTP'][p['Index']]=p['Y1']
            p['TP_flag']=1
            if p['Case_flag']==1:
                p['Index']=p['Index']+1
        else:
            p['X_LTP'][p['Index']]=p['X1']
            p['Y_LTP'][p['Index']]=p['Y1']
            p['TP_flag']=0
            if p['Case_flag']==2:
                p['Index']=p['Index']+1

    if p['TP_flag']==0:
        left=1.0
        right=p['Index']
        while left<=right:
            mid=(math.floor((left+right)/2)).astype(np.float32)
            if p['X_RTP'][mid]>=p['Action']: 
                left = mid + 1.0
            else:
                right = mid - 1.0
    else:
        left=1
        right=p['Index']
        while left<=right:
            mid=(math.floor((left+right)/2)).astype(np.float32)
            if p['X_LTP'][mid]<=p['Action']:
                left = mid + 1.0
            else:
                right = mid - 1.0

    p['Index'] = right

    if p['Index']==0:
        if p['TP_flag']==0:
            p['Case_flag']=1
            p['X0']=-p['Action']
            p['V0']=abs(p['Action'])
            p['Y0']=Displacement(d_init, Alpha,p['V0'],p['X0'],p['Case_flag'])
            p['X_LTP'][0]=p['X0']
            p['Y_LTP'][0]=Displacement(d_init, Alpha,p['V0'],p['X0'],p['Case_flag'])
        else:
            p['Case_flag']=2
            p['X0']=-p['Action'] 
            p['V0']=abs(p['Action'])
            p['Y0']=Displacement(d_init, Alpha,p['V0'],p['X0'],p['Case_flag'])
            p['X_RTP'][0]=p['X0']
            p['Y_RTP'][0]=Displacement(d_init, Alpha,p['V0'],p['X0'],p['Case_flag'])

    if p['Case_flag']==1:
        if p['TP_flag']==0:
            if p['Index']==0:
                D=Displacement(d_init, Alpha,p['V0'],p['Action'],p['Case_flag']); 
            else:
                D=p['Y_LTP'][p['Index']+1]-p['Y0']+Displacement(d_init, Alpha,p['V0'],p['X0']-p['X_LTP'][p['Index']+1]+p['Action'],p['Case_flag'])
        else:
            D=p['Y_RTP'][p['Index']]+p['Y0']-Displacement(d_init, Alpha,p['V0'],p['X0']+p['X_RTP'][p['Index']]-p['Action'],p['Case_flag'])        
    elif p['Case_flag']==2:
        if p['TP_flag']==0:
            D=p['Y_LTP'][p['Index']]+p['Y0']-Displacement(d_init, Alpha,p['V0'],p['X0']+p['X_LTP'][p['Index']]-p['Action'],p['Case_flag'])
        else:
            if p['Index']==0:
                D=Displacement(d_init, Alpha,p['V0'],p['Action'],p['Case_flag'])
            else:
                D=p['Y_RTP'][p['Index']+1]-p['Y0']+Displacement(d_init, Alpha,p['V0'],p['X0']-p['X_RTP'][p['Index']+1]+p['Action'],p['Case_flag'])
    return D


def Displacement(d_init, Alpha, V0, Input, Case_flag):
    d_out = 0.018
    h = 105e-6
    if Case_flag == 1:
        D=d_out*(Input/h*(d_init+Alpha*V0/h)-Alpha/(2*h**2)*(V0**2-Input**2))
    elif Case_flag == 2:
        D=d_out*(Input/h*(d_init+Alpha*V0/h)+Alpha/(2*h**2)*(V0**2-Input**2))

    return D        

def focal_power(n, d1, d2):
    d_out = 0.018
    d_innen = 0.012
    s = 155e-6
    F = 0.0

    if d1*d2>0.0 and d1<0.0:
        F = n/d_innen*math.sqrt(12*abs(d1+d2)/d_out)
    else: 
        F = n*(d1-d2)/(s*d_out)
    return F