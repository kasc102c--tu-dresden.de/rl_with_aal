import numpy as np 
from gym_AAL.functions import focal_power, displacement_one_piezo

from gym import Env, spaces

class AAL(Env):
    def __init__(self, num_steps:int = 500):
        super(AAL, self).__init__()
        self.num_steps = num_steps

        # Deine oberservation space
        self.observation_space = spaces.Box(
            low = np.array([-100.0, -30.0, -30.0, -30.0, -30.0]), 
            high = np.array([100.0, 120.0, 120.0, 120.0, 120.0]),
            dtype = np.float16)

        #Define action space
        self.action_space = spaces.Box(
            low = np.array([-10.0, -10.0, -10.0, -10.0]), 
            high = np.array([10.0, 10.0, 10.0, 10.0]),
            dtype = np.float16)

        #Define voltages
        self.V1 = 0.0
        self.V2 = 0.0
        self.V3 = 0.0
        self.V4 = 0.0

        #Define chromatic aberration
        self.current_ca = 0.0 
        self.steps_done = 0
        self.success = False

        #Define refractive indices
        self.n1_532 = 0.315
        self.n1_635 = 0.313
        self.n2_532 = 0.481
        self.n2_635 = 0.477

        #Define Piezo dicts
        self.p1 = {
            'Action' : 0,
            'V0' : 0,
            'X0' : 0,
            'Y0' : 0,
            'Y1' : 0,
            'X1' : 0,
            'X2' : 0,
            'X_LTP' : np.zeros(self.num_steps),
            'Y_LTP' : np.zeros(self.num_steps),
            'X_RTP' : np.zeros(self.num_steps),
            'Y_RTP' : np.zeros(self.num_steps),
            'Index' : 0,
            'Startup_flag' : 0,
            'TP_flag' : 0,
            'Case_flag' : 0
        }

        self.p2 = {
            'Action' : 0,
            'V0' : 0,
            'X0' : 0,
            'Y0' : 0,
            'Y1' : 0,
            'X1' : 0,
            'X2' : 0,
            'X_LTP' : np.zeros(self.num_steps),
            'Y_LTP' : np.zeros(self.num_steps),
            'X_RTP' : np.zeros(self.num_steps),
            'Y_RTP' : np.zeros(self.num_steps),
            'Index' : 0,
            'Startup_flag' : 0,
            'TP_flag' : 0,
            'Case_flag' : 0
        }

        self.p3 = {
            'Action' : 0,
            'V0' : 0,
            'X0' : 0,
            'Y0' : 0,
            'Y1' : 0,
            'X1' : 0,
            'X2' : 0,
            'X_LTP' : np.zeros(self.num_steps),
            'Y_LTP' : np.zeros(self.num_steps),
            'X_RTP' : np.zeros(self.num_steps),
            'Y_RTP' : np.zeros(self.num_steps),
            'Index' : 0,
            'Startup_flag' : 0,
            'TP_flag' : 0,
            'Case_flag' : 0
        }

        self.p4 = {
            'Action' : 0,
            'V0' : 0,
            'X0' : 0,
            'Y0' : 0,
            'Y1' : 0,
            'X1' : 0,
            'X2' : 0,
            'X_LTP' : np.zeros(self.num_steps),
            'Y_LTP' : np.zeros(self.num_steps),
            'X_RTP' : np.zeros(self.num_steps),
            'Y_RTP' : np.zeros(self.num_steps),
            'Index' : 0,
            'Startup_flag' : 0,
            'TP_flag' : 0,
            'Case_flag' : 0
        }

    def _get_obs(self):
        return np.array([self.ca, self.V1, self.V2, self.V3, self.V4]).astype(np.float16)

    def _get_info(self):
        return {"Step": self.steps_done, "is_success": self.success}        

    def reset(self):
        #init random chromatic aberration
        self.sys_532 = np.random.uniform(low = -2.0, high = 2.0)
        self.sys_635 = np.random.uniform(low = -2.0, high = 2.0)
        self.ca = self.sys_532-self.sys_635
        #init voltages
        self.V1 = 0.0
        self.V2 = 0.0
        self.V3 = 0.0
        self.V4 = 0.0
        #init piezos as dicts
        self.steps_done = 0
        self.success = False

        piezos = [self.p1, self.p2, self.p3, self.p4]
        for p in piezos:
            p['Action'] = 0.0
            p['V0'] = 0.0
            p['X0'] = 0.0
            p['Y0'] = 0.0
            p['Y1'] = 0.0
            p['X1'] = 0.0
            p['X2'] = 0.0
            p['X_LTP'] = np.zeros(self.num_steps)
            p['Y_LTP'] = np.zeros(self.num_steps)
            p['X_RTP'] = np.zeros(self.num_steps)
            p['Y_RTP'] = np.zeros(self.num_steps)
            p['Index'] = 0
            p['Startup_flag'] = 0
            p['TP_flag'] = 0
            p['Case_flag'] = 0 

        obs = self._get_obs()
        return obs


    def step(self, action: np.ndarray):
        #reset reward and done signal for this step
        self.steps_done = self.steps_done + 1
        done = False
        reward = 0.0

        #get new voltages
        self.V1 = self.V1 + action[0]
        self.V2 = self.V2 + action[1]
        self.V3 = self.V3 + action[2]
        self.V4 = self.V4 + action[3]

        if self.V1<-30.0:
            self.V1 = -30.0
            reward = reward - 1.0
        if self.V1>120.0:
            self.V1 = 120.0
            reward = reward - 1.0

        if self.V2<-30.0:
            self.V2 = -30.0
            reward = reward - 1.0
        if self.V2>120.0:
            self.V2 = 120.0
            reward = reward - 1.0

        if self.V3<-30.0:
            self.V3 = -30.0
            reward = reward - 1.0
        if self.V3>120.0:
            self.V3 = 120.0
            reward = reward - 1.0

        if self.V4<-30.0:
            self.V4 = -30.0
            reward = reward - 1.0
        if self.V4>120.0:
            self.V4 = 120.0
            reward = reward - 1.0            

        self.p1['Action'] = self.V1
        self.p2['Action'] = self.V2
        self.p3['Action'] = self.V3
        self.p4['Action'] = self.V4

        #calculate new chromatic aberration
        d1 = displacement_one_piezo(self.p1)
        d2 = displacement_one_piezo(self.p2)
        d3 = displacement_one_piezo(self.p3)
        d4 = displacement_one_piezo(self.p4)

        F_1_635 = focal_power(self.n1_635, d1, d2)
        F_1_532 = focal_power(self.n1_532, d1, d2)

        F_2_532 = -1.0*focal_power(self.n2_532, d3, d4)
        F_2_635 = -1.0*focal_power(self.n2_635, d3, d4)

        F_532 = self.sys_532+F_1_532+F_2_532
        F_635 = self.sys_635+F_1_635+F_2_635
        ca = F_532-F_635

        if abs(self.ca) > abs(ca):
            reward = reward + 1.0
        elif abs(self.ca) < abs(ca):
            reward = reward - 1.0

        #return new ca and check terminal condition
        self.ca = ca
        if abs(self.ca) < 0.05:
            reward = reward + 500.0
        done = bool(abs(self.ca) < 0.05)
        self.success = done

        obs = self._get_obs()
        info = self._get_info()

        return obs, reward, done, info     
    
