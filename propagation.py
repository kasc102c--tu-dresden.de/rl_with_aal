from ctypes import sizeof
import numpy as np
from scipy import ndimage, signal
from skimage.measure import regionprops, regionprops_table, label
from skimage.morphology import convex_hull_image, dilation, erosion, disk
import cv2
import math
import cmath
import matplotlib.pyplot as plt

def evaluate(h, dist_start: float = 0.0, dist_step: float = 0.1, dist_end: float = 1.0, plot_overview: bool = False, plot_details: bool = False):
    wl_1 = 532e-9 #set wavelength for green laser
    wl_2 = 635e-9 #set wavelength for red laser
    dx_1 = 4.399e-6 #set pixel size for green laser
    dx_2 = 4.375e-6 #set pixel size for red laser
    ### known spectrum positions ###
    x_1 = 0 #1615 # X position for green laser
    y_1 = 0 #1280 # Y position for green laser
    x_2 = 0 #802 # X position for red laser
    y_2 = 0 #1298 # Y position for red laser
    print('Green')
    d_532 = propagate(h, wl_1, dist_start, dist_step, dist_end, dx_1, x_1, y_1, plot_details) #call propagation function
    z_532 = d_532.argmin() # find minimum element to derive axial focal spot position
    f_532 = dist_start+z_532*dist_step # calulate absolute value of axial focal spot position
    #print(f'Axial focal spot position of 532 nm: {f_532} m')
    print('Red')
    d_635 = propagate(h, wl_2, dist_start, dist_step, dist_end, dx_2, x_2, y_2, plot_details)
    z_635 = d_635.argmin()
    f_635 = dist_start+z_635*dist_step
    #print(f'Axial focal spot position of 635 nm: {f_635} m')

    ca = f_532-f_635 # calculate chromatic aberration in m

    if plot_overview == True: # plot radius of amplitude over propagation distance for checking
        fig, ax = plt.subplots()
        dist = np.arange(dist_start, dist_end, dist_step)
        ax.plot(dist, d_532, label="532 nm", color = 'tab:green')
        ax.plot(dist, d_635, label="635 nm", color = 'tab:red')
        ax.set_title("Propagation_result")
        ax.legend()
        ax.grid()
        ax.set_ylabel('Diamater of Amplitude [px]')
        ax.set_xlabel('Propagation distance [m]')
        plt.show()

    return f_532, f_635, ca

def propagate(h, wl, dist_start: float = 0.0, dist_step: float = 0.1, dist_end: float = 1.0, dx: float = 4.4e-6, spectrum_cent_x:int = 0, spectrum_cent_y:int = 0, plot: bool = False):
    dy = dx
    filt = 10#8#10#20#15 #filter for convolution
    alpha = 35#30#25 # necessary for gaussian window for filtering ## on 20.09.22 this value was set to 25 instead of 7 to enlarge the gaussian window!
    boundary_size = 150#200 # boundary size for fourier space spectrum cutting --> maybe not necessary anymore?

    Ny, Nx = h.shape
    print('Ny=',Ny,'Nx=',Nx)
    k = 2*math.pi/wl
    H = h-signal.convolve2d(h, np.ones((filt, filt)), mode='same')/filt**2
    fft_H = np.fft.fftshift(np.fft.fft2(H)) # transform fourier space

    temp = abs(fft_H)
    binary_temp = temp.copy()

    #Excahnge Ny and Nx//27.09.22 Ning Guo
    binary_temp[:,1:boundary_size] = 0
    binary_temp[:,int(Nx-boundary_size):-1] = 0

    ##################Excahnge Ny and Nx//27.09.22 Ning Guo
    binary_temp[1:boundary_size,:] = 0
    binary_temp[int(Ny-boundary_size):-1,:] = 0

    if wl == 532e-9: # cut off red spectrum, green left//cut green spectrum?????
        #Excahnge Ny and Nx//27.09.22 Ning Guo
        binary_temp[1:int(Ny/2),:] = 0
        binary_temp[:,1:int(Nx/2)] = 0
    elif wl == 635e-9: # cut off green spectrum, red left//cut red spectrum?????
        #Excahnge Ny and Nx//27.09.22 Ning Guo
        binary_temp[1:int(Ny/2),:] = 0
        #binary_temp[int(Nx/2):-1,:] = 0
        binary_temp[:,int(Nx/2):-1] = 0
    else: 
        print("Error: Wavelength doesn't match spectrum cutting!")

    ### new, faster solution: 
    (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(binary_temp)
    r = 1

    while maxLoc ==(0,0):
        r = r+2
        gray = cv2.GaussianBlur(binary_temp.copy(), (r, r), 0)
        (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)

    spectrum_cent_x, spectrum_cent_y = maxLoc

    if plot ==True: # plot detected spectrum to check if correct --> yes, it's correct, no error up to here
        fig, ax = plt.subplots()
        ax.imshow(binary_temp, cmap = 'gray')
        circle = plt.Circle((spectrum_cent_x, spectrum_cent_y), 50, color='r', fill=False)
        ax.add_patch(circle)
        ax.set_title("Detected Spectrum")
        plt.show()

    fft_H = np.roll(fft_H, (int(Nx/2-spectrum_cent_x), int(Ny/2-spectrum_cent_y)), axis = (1, 0)) # shift detected spectrum to the middle

    # gaussian window for filtering
    mask_size = max(fft_H.shape)
    w = signal.windows.gaussian(mask_size, alpha, sym = True)
    w = np.expand_dims(w, axis = 1)
    ww_x = np.repeat(w, mask_size, axis = 1)
    w_y = (w.conj()).transpose()
    ww_y = np.repeat(w_y, mask_size, axis = 0)
    mask = np.multiply(ww_x,ww_y)
    mask[mask<0.005] = 0 # FFT peak is far too big, if mask is not 0 there, zero order is somewhat retained.
    mask = mask[int(Nx/2-Ny/2):int(Nx/2+Ny/2),:]

    fft_H = np.multiply(fft_H, mask)
    x, y = np.meshgrid(np.arange(start=-Nx/2, stop=Nx/2) ,np.arange(start=-Ny/2, stop=Ny/2))
    r = np.divide(2*math.pi*x,dx*Nx)**2 + np.divide(2*math.pi*y, dy*Ny)**2

    del mask, w, ww_x, ww_y, w_y

    # propagation over the distances in "dist"
    if plot == True:
        min_amplitude = np.zeros((Ny, Nx))
    dist = np.arange(start = dist_start, stop = dist_end, step = dist_step)
    element = cv2.getStructuringElement(shape=cv2.MORPH_RECT, ksize=(5, 5))
    a = np.zeros((Ny, Nx, len(dist))) # collect all the amplitude images
    c = np.zeros(len(dist)) # collect the diameters of the bright spot in amplitude images
    for i in range(len(dist)):
        kernel = np.exp(-1j*np.sqrt(k**2-r)*dist[i])
        fft_HH = np.multiply(fft_H[:,:],kernel)
        fft_HH = np.fft.fftshift(fft_HH)
        Ud = np.fft.ifft2(fft_HH)
        Id = abs(Ud) # amplitude of reconstructed wavefront
        #p = cmath.phase(Ud)

        if plot ==True: # plot amplitude images for each distance
            fig, ax = plt.subplots()
            ax.imshow(Id, cmap = 'gray')
            ax.set_title(f"Reconstructed Amplitude at {dist[i]} m")
            ax.axis('off')
            plt.show()

        a[:,:,i] = Id # add amplitude image to array
        t = 0.3*Id.max() # find threshold to derive the bright spot in the amplitude image
        bw = Id>t
        bw = dilation(bw, disk(30))
        bw = np.uint8(bw)


        morph_img = bw.copy()
        cv2.morphologyEx(src=bw, op=cv2.MORPH_CLOSE, kernel=element, dst=morph_img)

        contours,_ = cv2.findContours(morph_img,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        areas = [cv2.contourArea(c) for c in contours]
        sorted_areas = np.sort(areas)

        cnt=contours[areas.index(sorted_areas[-1])] #the biggest contour

         #Because the contour is a tuple, its element 0 stores the useful data
            #print(contours,type(contours))
        (center_x, center_y),radius = cv2.minEnclosingCircle(cnt)
        diameter = int(2*radius)


        c[i] = diameter # save diameter for each propagation distance
        if (plot == True and c[i]<c[i-1]) or (plot==True and i==len(dist)):
            min_amplitude = bw
            middle_x = int(center_x)
            middle_y = int(center_y)
        
        del bw, morph_img, contours, areas, sorted_areas, cnt, kernel

    if plot ==True: # plot the propagation slices (same image as in ICO presentation)
        img_a = a[round(Ny/2),:,:]
        fig, ax = plt.subplots()
        ax.imshow(img_a, cmap = 'gray')
        ax.set_title("Propagation")
        ax.axis('off')
        plt.show()

        fi, ax = plt.subplots() # show full reconstructed amplitude image for each distance
        ax.imshow(min_amplitude, cmap='gray', aspect='auto')
        ax.set_title('Amplitude')
        circle = plt.Circle((middle_x, middle_y), int(c.min()/2), color='r', fill=False)
        ax.add_patch(circle)
        ax.axis('off')
        plt.show()

    return c

def bwareafilt(img): # custom function inspired by MatLab function bwareafilt
    img = np.array(img, dtype=np.uint8)
    num_labels, labels, s, centroids = cv2.connectedComponentsWithStats(
        img, connectivity=8)
    max_size = 0
    for i in range(num_labels):
            label_size = s[i, cv2.CC_STAT_AREA]
            if label_size > max_size:
                binary_one = np.zeros_like(img)
                binary_one[labels == i] = 1
    return binary_one