import gym
import gym_AAL

import torch.nn as nn

from stable_baselines3.common.monitor import Monitor
from stable_baselines3.common.env_util import make_vec_env
from stable_baselines3.common.callbacks import EvalCallback, StopTrainingOnNoModelImprovement

from sb3_contrib import RecurrentPPO

log_dir = "./train_results/"
eval_dir = "./eval_results/"
model_dir = "./model"
n_timesteps = 5e5

# train env
#env = gym.make('AAL-v0')
env_id = "AAL-v0"
env = make_vec_env(env_id, n_envs = 8)

# eval env
eval_env = Monitor(gym.make('AAL-v0'))

# train model
model = RecurrentPPO(
        "MlpLstmPolicy",
        env,
        n_steps = 128,
        learning_rate = 1e-3,
        #verbose=1,
        batch_size = 256,
        seed = 0,
        clip_range = 0.2,
        n_epochs = 10,
        max_grad_norm = 1,
        gamma = 0.95,
        gae_lambda = 0.95,
        tensorboard_log= log_dir,
        policy_kwargs=dict(
            activation_fn = nn.ReLU,
            net_arch=[1, dict(pi=[1], vf=[1])],
            lstm_hidden_size=64,
            ortho_init=False,
            enable_critic_lstm=False,
        ),
    )

print(model.policy)

callback_until_best = StopTrainingOnNoModelImprovement(
    max_no_improvement_evals = 5, 
    min_evals= 15, 
    verbose = 1)
eval_callback = EvalCallback(
    eval_env = eval_env, 
    callback_on_new_best= callback_until_best, 
    eval_freq = 1250, 
    n_eval_episodes=50,
    log_path= eval_dir,
    best_model_save_path= model_dir, 
    verbose = 1)
model.learn(
    total_timesteps = n_timesteps, 
    callback= eval_callback, 
    )

model.save(model_dir)