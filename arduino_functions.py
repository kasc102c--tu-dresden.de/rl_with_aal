def VoltageToPWM(V:int):
    if V>=150:
        return 0.5
    elif V<=-50:
        return 2.1
    else: 
        return (0.7-(1/135)*(V-132))